Semua pernyataan mengenai HTML ini benar kecuali ... 
A. untuk membuat halaman web
B. bahasa pemrograman
C. diakses melalui javascript
D. dapat dijalankan dari chrome atau safari
ANSWER: B

Kegunaan form adalah ...
A. memperoleh informasi pembelian secara online
B. untuk mencetak miring dalam pembuatan web
C. untuk melihat data-data yang tersedia
D. untuk dapat mengakses internet lebih cepat
ANSWER: A

Perintah untuk menyatukan 3 baris pada sebuah tabel adalah ...
A. <td colspan=3>
B. <td colspan=”3”>
C. <td rowspan=3>
D. <td rowspan=”3”>
ANSWER: C

Apa perintah yang paling pertama dalam mengawali pembuatan HTML?
A. <body>
B. </body>
C. </html>
D. <html>
ANSWER: D

Ciri utama dari HTML adalah ...
A. Adanya Tag dan Body
B. Adanya Tag dan Elemen
C. Adanya Tag dan Head
D. Adanya Head dan Elemen
ANSWER: B

CSS merupakan singkatan dari ...
A. Conversion Style Sheet
B. Conversion Sheet Style
C. Cascading Sheet Style
D. Cascading Style Sheet
ANSWER: D
 
Website yang tampilannya bisa menyesuaikan konten tergantung situasi merupakan jenis website ...
A. statis
B. powerful
C. dinamis
D. incognito
ANSWER: C

Pada umumnya file .html digunakan untuk ...
A. membuat sebuah halaman web
B. membuat perintah web
C. menampilkan data web
D. Semua jawaban benar
ANSWER: A

Jika kita ingin membuat paragraf di HTML maka perintahnya adalah ...
A. <li> "paragraf" </li>
B. <input type="paragraf"></input>
C. <p> "paragraf" </p>
D. <body> "paragraf" <body>
ANSWER: C

Berikut ini merupakan aplikasi untuk membuat file html kecuali ...
A. notepad
B. sticky note
C. notepad ++
D. sublime text
ANSWER: B



